_La siguiente documentación es una versión resumida de la página oficial de Devilbox y se centrará en la instalación y configuración de los contenedores para trabajo con varios proyectos al mismo tiempo._

> Se puede acceder a la documentación original directamente [haciendo click acá](https://devilbox.readthedocs.io/en/latest/getting-started/install-the-devilbox.html)

___
## Instalación inicial

```bash
> git clone https://github.com/cytopia/devilbox 
```

Ingresar en la carpeta de Devilbox  
Crear archivo .env

```bash
> cp env-example .env
```
____
## Configurar archivo .env

> Esta documentación apunta a tener un servidor Apache 2.4 | PHP 5.6/7.2 | MySQL 5.7  
> Por lo cual, nos centraremos en modificar sólo esos valores.

Obtener el usuario y el grupo actual para agregar a .env  

```bash
#Id del usuario
> id -u
#Id del grupo
> id -g
```

Modificar las siguientes lineas del archivo .env

```bash
#Usuario y grupo para los permisos
NEW_UID={uid}
NEW_GID={gid}

#PHP
PHP_SERVER=5.6
PHP_SERVER=7.2

#Apache
HTTPD_SERVER=apache-2.4

#MySQL
MYSQL_SERVER=mysql-5.7

#PostgreSQL
PGSQL_SERVER=9.6

#Redis
REDIS_SERVER=4.0

#Memcached
MEMCD_SERVER=1.5.5

#MongoDB
MONGO_SERVER=3.7

#A continuación configuramos la información que queremos persistir. 
#Principalmente la carpeta donde residen nuestros proyectos
HOST_PATH_HTTPD_DATADIR=../www
#NOTA: EN Docker for Windows / OSX es necesario ir a Docker y compartir el disco. 

#Base de datos MySQL
Queremos que quede afuera de la carpeta de devilbox para tener mas control sobre la misma, por lo cual la dirección seria
HOST_PATH_MYSQL_DATADIR=../data/mysql

HOST_PATH_PGSQL_DATADIR=../data/pgsql
HOST_PATH_MONGO_DATADIR=../data/mongo
```

## Generar las imágenes necesarias
Ejecutar los siguientes comandos dentro de la carpeta de Devilbox


```bash
# Detener los contenedores que podrían estar corriendo y los eliminamos
> docker-compose stop && docker-compose rm
# actualizamos a la última versión
> git pull origin master
# Actualizamos las imágenes
> docker-compose pull 

#iniciamos el servidor
> docker-compose up
```

### Para detener el el servidor simplemente ejecutar
```
> docker-compose stop
``` 